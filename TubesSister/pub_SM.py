# import paho mqtt
import paho.mqtt.client as mqtt
# import time untuk sleep()
import time
# import datetime untuk mendapatkan waktu dan tanggal
import datetime


########################################

#Membuat callback on_publish untuk publish data 
def on_publish(client, userdata, mid):
    
    print("on_publish, mid {}".format(mid))

########################################


# mendefinisikan nama broker yang akan digunakan
broker_address = "broker.emqx.io"
# membuat client baru bernama SM
print("Creating Client SM")
client = mqtt.Client("SM")
# mengkaitkan callback on_publish ke client
client.on_publish = on_publish
# mengkoneksikan ke broker
print("Connecting to broker . . .\n")
client.connect(broker_address, port=1883)

# mulai loop client
client.loop_start()
# Pesan
agency = "Agency : SMTOWN"
tema   = " Tema   : Korean Ballad"
artist = " Artist : EXO"
jadwal = datetime.datetime.strptime(
     input('Jadwal acara (Format : `YYYY/mm/dd/`) : '), "%Y/%m/%d"
    )
jam = input ('Jam acara (Format :  HH:MM`) : ')
msg = f"{agency}\n{tema}\n{artist} \n Jadwal : {jadwal.year}/{jadwal.month}/{jadwal.day} \n Pukul  : {jam}"
# Publish jadwal
print('Publishing message to topic . . .')
client.publish('topik_SM',msg)
# stop loop
client.loop_stop()
