# import paho mqtt
import paho.mqtt.client as mqtt
# import time for sleep()
import time

subs = []
########################################


   
   
#MEMBUAT CALLBACK on_message jika ada pesan.   
def on_message(client, userdata, message):
    
    print("\n\nReceived Message : \n", str(message.payload.decode("utf-8")))

 
#MEMBUAT MENU UNTUK SUBSCRIBE / UNSUBSCRIBE
def menu(client):
   
    SM = "topik_SM"
    YG = "topik_YG"
    print(f"\nSubscribing to topic . . . {subs}")
    print("Subscribe/Unsubscribe : \n 1. SMTOWN \n 2. YG Entertainment")
    command = input('Pilihan : ')
    if command == str(1):
        if SM in subs:  # Jika topik sudah disubscribe, maka topik di pop dan client unsubscribe
            subs.pop(subs.index(SM))
            client.unsubscribe(SM)
        else:
            subs.append(SM)
            client.subscribe(SM)
    elif command == str(2):
        if YG in subs:  # Jika topik sudah disubscribe, maka topik di pop dan client unsubscribe
            subs.pop(subs.index(YG))
            client.unsubscribe(YG)
        else:
            subs.append(YG)
            client.subscribe(YG)

########################################


# membuat definisi nama broker yang akan digunakan
broker_address = "broker.emqx.io"

# membuat client baru bernama Tubes
print("Creating new instance . . .")
client = mqtt.Client("Tubes")

# membuat koneksi ke broker
print("Connecting to broker . . .")
client.connect(broker_address, port=1883)

# menjalankan loop client
client.loop_start()

# menjalankan menu
menu(client)

while True:
    print('\nKetik `menu` untuk kembali ke menu, ketik `exit` untuk keluar.')
    client.on_message = on_message
    inputs = input('Pilihan : ')
    if (inputs == "menu"):
        menu(client)
    elif (inputs == "exit"):
        break
    time.sleep(1)

# stop loop
print('Exit . . .')
print('Finish')
client.loop_stop()
